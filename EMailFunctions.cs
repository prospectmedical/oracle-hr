﻿using System.Threading;

namespace OracleHR
{
	class EMailFunctions
	{
		public string Logo01PathFileName { get; set; }
		public string Attachment01PathFileName { get; set; }
		private static string msErrorMessage;

		public string ErrorMessage
		{
			get
			{
				return msErrorMessage;
			}
		}

		public string DoNoTReply()
		{
			PublicRoutine oPublicRoutine = new PublicRoutine();
			return oPublicRoutine.ASPCRLF(5) + "***** NOTE: Do not reply to this e-mail!" + oPublicRoutine.ASPCRLF(1) + oPublicRoutine.ASPSpace(23) + "This Email address is not monitored." + oPublicRoutine.ASPCRLF(1);
		}
		
		public bool Send(string Recipient, string Subject, string EMailMessage, bool IncludeBCC, string  ImageTag = null)
		{
			Message oMessage = new Message();

			if (!Global.SendEMail) { return true; }
			if (!oMessage.Sender(Global.EMailSender))
			{
				msErrorMessage = "Send eMail error setting Sender";
				return false;
			}
			if (!oMessage.Recipient(Recipient))
			{
				msErrorMessage = "Send eMail error setting Recipient";
				return false;
			}
			if (IncludeBCC)
			{
				if (!oMessage.BCC(Global.BCC))
				{
					msErrorMessage = "Send eMail error setting BCC";
					return false;
				}
			}
			if (!oMessage.Subject(Subject))
			{
				msErrorMessage = "Send eMail error setting Subject";
				return false;
			}
			if (!oMessage.MessageText(EMailMessage))
			{
				msErrorMessage = "Send eMail error setting MessageText";
				return false;
			}
			if (Attachment01PathFileName != null && Attachment01PathFileName.Length > 0)
			{
				if (!oMessage.AddAttachment(Attachment01PathFileName))
				{
					msErrorMessage = "Send eMail error adding attachment 01";
					return false;
				}
			}
			if (Logo01PathFileName != null && Logo01PathFileName.Length > 0)
			{
				if (!oMessage.Logo01PathFileName(Logo01PathFileName))
				{
					msErrorMessage = "Send eMail error adding logo 01";
					return false;
				}
			}
			if (!oMessage.Send(ImageTag))
			{
				msErrorMessage = "Send eMail error during send function";
				return false;
			}
			Thread.Sleep(Global.EMailPauseTime);
			return true;
		}

			//
	}
}
