﻿using System.Collections;

namespace OracleHR
{
	public static class Global
	{
		public static string ApplicationName = "Oracle HR";
		public static string ApplicationPath;
		public static string HTMLPath;
		public static string GraphicsPath;
		public static string LogPath;
		public static string LogFileName;
		public static bool TraceEnabled;
		public static int ServicePauseTime;
		public static ArrayList EMailRecipient;
		public static string EMailSender;
		public static bool SendEMail;
		public static string WindowsUserID;
		public static string Version;
		public static string NorthPointPath;
		public static string NorthPointFilePattern;

		public const string EventLogName = "Oracle HR";
		public const string INIFileName = "OracleHR.ini";
		public const int EMailPauseTime = 2000;
		public const string BCC = "OracleHR@Crozer.org";
		public const char DoubleQuote = (char)34;
		public const string SMTPAddress = "smtp.crozer.org";
		//public static string LogPath = @"C:\Service Logs\Oracle HR\";
	}
}
