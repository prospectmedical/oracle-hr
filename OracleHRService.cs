﻿using System;
using System.Globalization;
using System.IO;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using System.Reflection;
using System.Linq;
using System.Xml.Linq;

namespace OracleHR
{
	public partial class OracleHRService : ServiceBase
	{
		Thread moThread;
		TextWriterTraceListener moTextWriterTraceListener;
		PublicRoutine moPublicRoutine = new PublicRoutine();

		#region "Routines"
		private string RetrieveElement(XElement oXElement, string FieldName)
		{
			string ReturnValue;
			try
			{
				ReturnValue = oXElement.Element(FieldName).Value;
			}
			catch
			{
				ReturnValue = null;
			}
			return ReturnValue;
		}
		#endregion

		public OracleHRService()
		{
			InitializeComponent();
			CanPauseAndContinue = true;
			ServiceName = "OracleHR";
			StartService();

			// Test
			//StartUpEMail();
			//LoadData01();
			//ShutdownEMail();
			//Trace.Close();
			//Environment.Exit(0);
			//Test
					 
			//ServiceProcessMethod();
		}

		private void StartService()
		{
			String sRoutine = "StartService";
			InitializeApplication oInitializeApplication = new InitializeApplication();
			//ServiceBase oServiceBase;

			Global.ApplicationPath = moPublicRoutine.ApplicationStartupPath();
			Global.HTMLPath = Global.ApplicationPath + @"HTML\";
			Global.GraphicsPath = Global.ApplicationPath + @"Graphics\";
			if (!oInitializeApplication.GetSettings())
			{
				moPublicRoutine.WriteEventLog("InitializeApplication", "Error: Service initialization failed.", EventLogEntryType.Error);
				System.Environment.Exit(0);
			}
			Global.LogPath = moPublicRoutine.AddBackslash(oInitializeApplication.LogPath);
			if (!Directory.Exists(Global.LogPath)) { Directory.CreateDirectory(Global.LogPath); };
			Global.LogFileName = Global.ApplicationName + " Log " + DateTime.Now.ToString("yyyyMMdd HHmmss") + ".txt";
			moTextWriterTraceListener = new TextWriterTraceListener(Global.LogPath + Global.LogFileName);
			Trace.Listeners.Add(moTextWriterTraceListener);
			Trace.AutoFlush = true;
			Global.WindowsUserID = Environment.UserName;
			Assembly oAssembly = Assembly.GetExecutingAssembly();
			FileVersionInfo oFileVersionInfo = FileVersionInfo.GetVersionInfo(oAssembly.Location);
			Global.Version = oFileVersionInfo.FileVersion;
			Global.TraceEnabled = oInitializeApplication.TraceEnabled;
			Global.ServicePauseTime = Convert.ToInt32(oInitializeApplication.ServicePauseTime * 60000);
			Global.EMailRecipient = oInitializeApplication.EMailRecipient;
			Global.EMailSender = oInitializeApplication.EMailSender;
			Global.SendEMail = oInitializeApplication.SendEmail;
			Global.NorthPointPath = oInitializeApplication.NorthPointPath;
			Global.NorthPointFilePattern = oInitializeApplication.NorthPointFilePattern;

			moPublicRoutine.TraceWrite("Startup.Run was initialized...", sRoutine);
			moPublicRoutine.TraceWrite("Application Name: " + Global.ApplicationName, sRoutine);
			moPublicRoutine.TraceWrite("Windows UserID: " + Global.WindowsUserID, sRoutine);
			moPublicRoutine.TraceWrite("Application Version: " + Global.Version, sRoutine);
			moPublicRoutine.TraceWrite("Application Startup Path: " + Global.ApplicationPath, sRoutine);
			if (Global.TraceEnabled)
			{
				moPublicRoutine.TraceWrite("Trace: True", sRoutine);
			}
			else
			{
				moPublicRoutine.TraceWrite("Trace: False", sRoutine);
			}
			moPublicRoutine.TraceWrite("Service Pause Time: " + String.Format("{0:#,##0;(#,##0);0}", Global.ServicePauseTime / 60000) + " minutes", sRoutine);
			if (Global.EMailRecipient.Count > 0)
			{
				for (int iWork = 0; iWork < Global.EMailRecipient.Count; iWork += 1)
				{
					moPublicRoutine.TraceWrite("EMail Recipient(" + String.Format("{0:#,##0;(#,##0);0}", iWork + 1) + "): " + Global.EMailRecipient[iWork].ToString(), sRoutine);
				}
			}
			else
			{
				moPublicRoutine.TraceWrite("EMail Recipient(0): ", sRoutine);
			}
			moPublicRoutine.TraceWrite("EMail Sender: " + Global.EMailSender, sRoutine);
			if (Global.SendEMail)
			{
				moPublicRoutine.TraceWrite("Send eMail: True", sRoutine);
			}
			else
			{
				moPublicRoutine.TraceWrite("Send eMail: False", sRoutine);
			}
			moPublicRoutine.TraceWrite("North Point Path: " + Global.NorthPointPath, sRoutine);
			moPublicRoutine.TraceWrite("North Point File Pattern: " + Global.NorthPointFilePattern, sRoutine);

			moPublicRoutine.TraceWrite(String.Empty.PadLeft(100, (char)127), sRoutine);
			moPublicRoutine.TraceWrite(String.Empty.PadLeft(100, (char)45), sRoutine);
			moPublicRoutine.TraceWrite(String.Empty.PadLeft(100, (char)127), sRoutine);
			moPublicRoutine.TraceWrite(" ", sRoutine);

		//	oServiceBase = new ServiceBase[] {new OracleHRService()};
		//	oServiceBase = new ServiceBase() { new OracleHRService() }
		//	ServiceBase.Run(oServiceBase);
		//WriteEventTrace("Service main method exiting...", sRoutine, EventLogEntryType.Information)
		//Trace.Listeners.Remove(InTextWriterTraceListener)
		//InTextWriterTraceListener.Close()
		//InTextWriterTraceListener = Nothing
		}

		private void ServiceProcessMethod()
		{
			String sRoutine = "ServiceProcessMethod";
			moPublicRoutine.TraceWrite("Starting the service process thread.", sRoutine);
			try
			{
				if (Global.SendEMail) {
					StartUpEMail();
				}
				moPublicRoutine.WriteEventTrace("Process Messages", sRoutine, EventLogEntryType.Information);
				do
				{
					//try
					//{
						LoadData01();
					//}
					//catch (Exception oException)
					//{
					//	moPublicRoutine.WriteEventTrace(oException.Message, sRoutine, EventLogEntryType.Information);
					//}
					moPublicRoutine.TraceWrite(String.Empty.PadLeft(100, (char)127), sRoutine);
					moPublicRoutine.TraceWrite(String.Empty.PadLeft(100, (char)45), sRoutine);
					moPublicRoutine.TraceWrite(String.Empty.PadLeft(100, (char)127), sRoutine);
					moPublicRoutine.TraceWrite(" ", sRoutine);
					Thread.Sleep(Global.ServicePauseTime);
				}
				while (true);

			}
			catch (Exception oException)
			{
				moPublicRoutine.WriteEventTrace("Thread abort signaled. Exception Message: " + oException.Message, sRoutine, EventLogEntryType.Information);
			}
			finally
			{
				moPublicRoutine.WriteEventTrace("Exiting the service process thread.", sRoutine, EventLogEntryType.Information);
				if (Global.SendEMail) { ShutdownEMail(); }
				Trace.Close();
				moThread.Join(TimeSpan.FromMilliseconds(500));
				moThread = null;
				this.Stop();
			}
		}

		protected override void OnStart(string[] args)
		{
			if (moThread == null || ((moThread.ThreadState & (System.Threading.ThreadState.Unstarted | System.Threading.ThreadState.Stopped)) != 0))
			{
				moPublicRoutine.WriteEventTrace("Starting the service process thread.", "OnStart", EventLogEntryType.Information);
				moThread = new Thread(new ThreadStart(ServiceProcessMethod));
				moThread.Start();
			}
			if (!(moThread == null))
			{
				moPublicRoutine.WriteEventTrace("Process thread state = " + moThread.ThreadState.ToString(), "OnStart", EventLogEntryType.Information);
			}
		}

		protected override void OnStop()
		{
			if (!(moThread == null) && moThread.IsAlive)
			{
				moPublicRoutine.WriteEventTrace("Stopping the service process thread.", "OnStop", EventLogEntryType.Information);
				moThread.Abort();
				moThread.Join(TimeSpan.FromMilliseconds(500));
			}
			if (!(moThread == null))
			{
				moPublicRoutine.WriteEventTrace("Process thread state = " + moThread.ThreadState.ToString(), "OnStop", EventLogEntryType.Information);
			}
		}

		protected override void OnCustomCommand(int Command)
		{
			base.OnCustomCommand(Command);
			moPublicRoutine.WriteEventTrace("Custom command received: " + Command.ToString(), "OnCustomCommand", EventLogEntryType.Information);
			switch (Command)
			{
				case 128:
					OnStop();
					break;

				case 129:
					OnStart(null);
					break;

				case 130:
					moPublicRoutine.WriteEventTrace("Process thread state = " + moThread.ThreadState.ToString(), "OnCustomCommand", EventLogEntryType.Information);
					moPublicRoutine.WriteEventTrace("Inrecognized custom command ignored!", "OnCustomCommand", EventLogEntryType.Information);
					break;
			}
		}

		private void StartUpEMail()
		{
			String sRoutine = "StartUpEMail";
			EMailFunctions oEMailFunctions = new EMailFunctions();
			string BodyText, InsertText;
			FileStream oFileStream = new FileStream(Global.HTMLPath + "Start01.html", FileMode.Open, FileAccess.Read);
			StreamReader oStreamReader = new StreamReader(oFileStream);
			DateTime CurrentDateTime = DateTime.Now;
			string ImageTag = CurrentDateTime.ToString("yyyyMMddHHmmss");
			DateTimeFormatInfo oDateTimeFormatInfo = new DateTimeFormatInfo();

			try
			{
				moPublicRoutine.TraceWrite("Start Send Startup eMail", sRoutine);
				if (Global.SendEMail && Global.EMailRecipient.Count > 0)
				{
					BodyText = oStreamReader.ReadToEnd();
					moPublicRoutine.CloseFileStream(oFileStream);
					moPublicRoutine.CloseStreamReader(oStreamReader);
					InsertText = Global.ApplicationName + " Startup";
					InsertText += moPublicRoutine.ASPCRLF(2) + moPublicRoutine.ASPSpace(5) + "Status: Started on " + oDateTimeFormatInfo.GetMonthName(CurrentDateTime.Month) + " ";
					InsertText +=   CurrentDateTime.Day.ToString() + @", " + CurrentDateTime.Year.ToString() + " at " + CurrentDateTime.ToString("HH:mm");
					BodyText = BodyText.Replace("[text01]", InsertText);
					BodyText = BodyText.Replace("[donotreply01]", oEMailFunctions.DoNoTReply());
					BodyText = BodyText.Replace("[imagetag]", ImageTag);
					for (int iWork = 0; iWork < Global.EMailRecipient.Count; iWork++) 
					{
						oEMailFunctions.Logo01PathFileName = Global.GraphicsPath + "OracleHR.png";
						if (!oEMailFunctions.Send(Global.EMailRecipient[iWork].ToString(), Global.ApplicationName + " Startup", BodyText, false, ImageTag))
						{
							moPublicRoutine.TraceWrite("Send startup eMail failed: " + oEMailFunctions.ErrorMessage, sRoutine);
						}
					}
				}
			}
			catch (Exception oException)
			{
				moPublicRoutine.TraceWrite("Send startup eMail failed: " + oException.Message, sRoutine);
			}
		}

		private void ShutdownEMail()
		{
			String sRoutine = "ShutdownEMail";
			EMailFunctions oEMailFunctions = new EMailFunctions();
			string BodyText, InsertText;
			FileStream oFileStream = new FileStream(Global.HTMLPath + "Shutdown01.html", FileMode.Open, FileAccess.Read);
			StreamReader oStreamReader = new StreamReader(oFileStream);
			DateTime CurrentDateTime = DateTime.Now;
			string ImageTag = CurrentDateTime.ToString("yyyyMMddHHmmss");
			DateTimeFormatInfo oDateTimeFormatInfo = new DateTimeFormatInfo();

			try
			{
				moPublicRoutine.TraceWrite("Start Send Shutdown eMail", sRoutine);
				if (Global.SendEMail && Global.EMailRecipient.Count > 0)
				{
					BodyText = oStreamReader.ReadToEnd();
					moPublicRoutine.CloseFileStream(oFileStream);
					moPublicRoutine.CloseStreamReader(oStreamReader);
					InsertText = Global.ApplicationName + " Shutdown";
					InsertText += moPublicRoutine.ASPCRLF(2) + moPublicRoutine.ASPSpace(5) + "Status: Shutdown on " + oDateTimeFormatInfo.GetMonthName(CurrentDateTime.Month) + " ";
					InsertText += CurrentDateTime.Day.ToString() + @", " + CurrentDateTime.Year.ToString() + " at " + CurrentDateTime.ToString("HH:mm");
					BodyText = BodyText.Replace("[text01]", InsertText);
					BodyText = BodyText.Replace("[donotreply01]", oEMailFunctions.DoNoTReply());
					BodyText = BodyText.Replace("[imagetag]", ImageTag);
					for (int iWork = 0; iWork < Global.EMailRecipient.Count; iWork++)
					{
						oEMailFunctions.Logo01PathFileName = Global.GraphicsPath + "OracleHR.png";
						if (!oEMailFunctions.Send(Global.EMailRecipient[iWork].ToString(), Global.ApplicationName + " Shutdown", BodyText, false, ImageTag))
						{
							moPublicRoutine.TraceWrite("Send shutdown eMail failed: " + oEMailFunctions.ErrorMessage, sRoutine);
						}
					}
				}
			}
			catch (Exception oException)
			{
				moPublicRoutine.TraceWrite("Send shutdown eMail failed: " + oException.Message, sRoutine);
			}
		}

		private void LoadData01()
		{
			String sRoutine = "LoadData01";
			try
			{
				string[] Files = Directory.GetFiles(Global.NorthPointPath, Global.NorthPointFilePattern);
				if (Files.Count() <= 0) {
					moPublicRoutine.TraceWrite("Load Data: No files found for processing.", sRoutine);
					return;
				}
				foreach (string File in Files) {
					moPublicRoutine.TraceWrite("Processing File: " + File, sRoutine);
					LoadData01A(File);
				}
			}
			catch (Exception oException)
			{
				moPublicRoutine.WriteEventTrace("Thread abort signaled. Exception Message: " + oException.Message, sRoutine, EventLogEntryType.Information);
			}
			finally { }
		}

		private bool LoadData01A(string PathFileName)
		{
			if (PathFileName == null) { return false; }
			FileInfo oFileInfo01 = new FileInfo(PathFileName);
			if (!oFileInfo01.Exists) { return false; }
			Processed oProcessed = new Processed();
			FileInfo oFileInfo02;
			string BackupFile, sWork;
			string[] ParseFields;
			int iWork;
			Employee oEmployee;
			WorkRelationship oWorkRelationship;
			Assignment oAssignment;
			int EmployeeRecordCount = 0;
			int WorkRelationshipRecordCount = 0;
			int AssignmentRecordCount = 0;
			Int64 iEmployeeNumber;

			oProcessed.ProcessedTypeID = 1;
			oProcessed.StartDateTime = DateTime.Now;
			XDocument oXDocument = XDocument.Load(PathFileName);
			oEmployee = new Employee();
			oEmployee.OpenDatabase();
			oWorkRelationship = new WorkRelationship();
			oWorkRelationship.OpenDatabase();
			oAssignment = new Assignment();
			oAssignment.OpenDatabase();
			foreach (XElement Employee in oXDocument.Descendants("Employee"))
			{
				EmployeeRecordCount += 1;
				oEmployee.InitializeData();
				oEmployee.PersonID = RetrieveElement(Employee, "PersonID");
				oEmployee.PersonNbr = RetrieveElement(Employee, "PersonNbr");
				iEmployeeNumber = 0;
				Int64.TryParse(oEmployee.PersonNbr.ToString(), out iEmployeeNumber);
				oEmployee.EmployeeNumber = iEmployeeNumber;
				oEmployee.FirstName = RetrieveElement(Employee, "FirstName");
				oEmployee.MiddleName = RetrieveElement(Employee, "MiddleName");
				oEmployee.LastName = RetrieveElement(Employee, "LastName");
				oEmployee.Title = RetrieveElement(Employee, "Title");
				oEmployee.FamiliarName = RetrieveElement(Employee, "FamiliarName");
				oEmployee.Suffix = RetrieveElement(Employee, "Suffix");
				oEmployee.LegacyEmployeeNumber = RetrieveElement(Employee, "LegacyEmployeeNumber");
				oEmployee.WorkEMail = RetrieveElement(Employee, "WorkEmail");
				oEmployee.WorkPhone = RetrieveElement(Employee, "WorkPhone");
				oEmployee.WorkMobilePhone = RetrieveElement(Employee, "WorkMobilePhone");
				if (!oEmployee.Update())
				{
					oEmployee.CloseDatabase();
					oWorkRelationship.CloseDatabase();
					oAssignment.CloseDatabase();
					return false;
				}
				foreach (XElement WorkRelationship in Employee.Elements("WorkRelationship"))
				{
					WorkRelationshipRecordCount += 1;
					oWorkRelationship.InitializeData();
					oWorkRelationship.WorkRelationshipID = RetrieveElement(WorkRelationship, "WorkRelationshipID");
					oWorkRelationship.PersonID = oEmployee.PersonID;
					oWorkRelationship.OriginalHireDate = RetrieveElement(WorkRelationship, "OriginalHireDate");
					oWorkRelationship.StartDate = RetrieveElement(WorkRelationship, "StartDate");
					oWorkRelationship.TerminationDate = RetrieveElement(WorkRelationship, "TerminationDate");
					if (!oWorkRelationship.Update())
					{
						oEmployee.CloseDatabase();
						oWorkRelationship.CloseDatabase();
						oAssignment.CloseDatabase();
						return false;
					}

					foreach (XElement Assignment in WorkRelationship.Elements("Assignment"))
					{
						AssignmentRecordCount += 1;
						oAssignment.InitializeData();
						oAssignment.AssignmentID = RetrieveElement(Assignment, "AssignmentID");
						oAssignment.PersonID = oEmployee.PersonID;
						oAssignment.EffectiveStartDate = RetrieveElement(Assignment, "EffectiveStartDate");
						oAssignment.AssignmentNbr = RetrieveElement(Assignment, "AssignmentNbr");
						oAssignment.PrimaryFlag = RetrieveElement(Assignment, "PrimaryFlag");
						oAssignment.AssignmentStatusCode = RetrieveElement(Assignment, "AssignmentStatusCode");
						oAssignment.AssignmentStatusName = RetrieveElement(Assignment, "AssignmentStatusName");
						oAssignment.EmploymentCategoryCode = RetrieveElement(Assignment, "EmploymentCategoryCode");
						oAssignment.EmploymentCategoryName = RetrieveElement(Assignment, "EmploymentCategoryName");
						oAssignment.JobCode = RetrieveElement(Assignment, "JobCode");
						oAssignment.JobName = RetrieveElement(Assignment, "JobName");
						oAssignment.DepartmentName = RetrieveElement(Assignment, "DepartmentName");
						oAssignment.LocationCode = RetrieveElement(Assignment, "LocationCode");
						oAssignment.LocationName = RetrieveElement(Assignment, "LocationName");
						oAssignment.ReportingCompany = RetrieveElement(Assignment, "ReportingCompany");
						oAssignment.PositionCode = RetrieveElement(Assignment, "PositionCode");
						oAssignment.CATSCompanyID = null;
						oAssignment.CATSDepartmentNumber = null;
						oAssignment.CATSCostCenter = null;
						oAssignment.CATSPositionNumber = null;
						sWork = oAssignment.PositionCode;
						if (sWork != null)
						{
							iWork = sWork.Where(x => x == '_').Count();
							if (iWork >= 3)
							{
								ParseFields = sWork.Split('_');
								oAssignment.CATSCompanyID = ParseFields[0];
								oAssignment.CATSDepartmentNumber = ParseFields[1];
								oAssignment.CATSCostCenter = ParseFields[1];
								oAssignment.CATSPositionNumber = ParseFields[2];
							}
						}
						oAssignment.BargainingUnitCode = RetrieveElement(Assignment, "BargainingUnitCode");
						if (!oAssignment.Update())
						{
							oEmployee.CloseDatabase();
							oWorkRelationship.CloseDatabase();
							oAssignment.CloseDatabase();
							return false;
						}
					}
				}
			}
			oEmployee.CloseDatabase();
			oWorkRelationship.CloseDatabase();
			oAssignment.CloseDatabase();
			BackupFile = Global.NorthPointPath + @"Processed\" + Path.GetFileNameWithoutExtension(oFileInfo01.Name) + DateTime.Now.ToString("_yyyyMMddHHmmssfff") + oFileInfo01.Extension;
			oFileInfo02 = new FileInfo(BackupFile);
			if (oFileInfo02.Exists)
			{
				oFileInfo02.Delete();
				if (oFileInfo02.Exists) { return false; }
			}
			oFileInfo01.MoveTo(BackupFile);
			oProcessed.PathFileName = PathFileName;
			oProcessed.BackupPathFileName = BackupFile;
			oProcessed.EmployeeRecordCount = EmployeeRecordCount;
			oProcessed.WorkRelationshipRecordCount = WorkRelationshipRecordCount;
			oProcessed.AssignmentRecordCount = AssignmentRecordCount;
			oProcessed.EndDateTime = DateTime.Now;
			oProcessed.OpenDatabase();
			oProcessed.Insert();
			oProcessed.CloseDatabase();
			return true;
		}

		//
	}
}
