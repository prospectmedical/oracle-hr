﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;

namespace OracleHR
{
	class DBRoutines
	{
		private string DataBaseConnectionString(string DataBaseUser, string DatabasePassword, string DatabaseName, string Server)
		{
			return "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=" + DataBaseUser + ";Password=" + DatabasePassword + ";Initial Catalog=" + DatabaseName +
						";Data Source=" + Server + ";Use Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;Workstation ID=AIApplication;Use Encryption for Data=False;" +
						"Tag with column collation when possible=False";
		}

		public bool OpenDatabase(OleDbConnection DatabaseConnection, int DatabaseToOpen)
		{
			switch (DatabaseToOpen)
			{
				case 1:
					DatabaseConnection.ConnectionString = DataBaseConnectionString("OracleHR", "OHR99963258", "OracleHR", "SQLDB04");
					break;
				default:
					return false;
			}
			try
			{
				DatabaseConnection.Open();
				return true;
			}
			catch
			{
				return false;
			}
		}

		public void CloseDB(OleDbConnection DatabaseConnection)
		{
			try
			{
				DatabaseConnection.Close();
				DatabaseConnection.Dispose();
				DatabaseConnection = null;
			}
			catch { }
		}
		public void CloseRDR(OleDbDataReader DataReader)
		{
			try
			{
				DataReader.Close();
				DataReader = null;
			}
			catch { }
		}
		public void CloseCMD(OleDbCommand DatabaseCommand)
		{
			try
			{
				DatabaseCommand.Dispose();
			}
			catch { }
		}





		//
	}
}
