﻿using System;
using System.Data;
using System.Data.OleDb;

namespace OracleHR
{
	class Employee
	{
		public string PersonID { get; set; }
		public string PersonNbr { get; set; }
		public Int64 EmployeeNumber { get; set; }
		public string FirstName { get; set; }
		public string MiddleName { get; set; }
		public string LastName { get; set; }
		public string Title { get; set; }
		public string FamiliarName { get; set; }
		public string Suffix { get; set; }
		public string LegacyEmployeeNumber { get; set; }
		public string WorkEMail { get; set; }
		public string WorkPhone { get; set; }
		public string WorkMobilePhone { get; set; }

		private OleDbConnection moDatabaseConnection;
		public string ErrorMessage { get; set; }

		public void InitializeData()
		{
			this.PersonID = null;
			this.PersonNbr = null;
			this.EmployeeNumber = 0;
			this.FirstName = null;
			this.MiddleName = null;
			this.LastName = null;
			this.Title = null;
			this.FamiliarName = null;
			this.Suffix = null;
			this.LegacyEmployeeNumber = null;
			this.WorkEMail = null;
			this.WorkPhone = null;
			this.WorkMobilePhone = null;
		}

		public bool OpenDatabase()
		{
			DBRoutines oDBRoutines = new DBRoutines();
			moDatabaseConnection = new OleDbConnection();
			return oDBRoutines.OpenDatabase(moDatabaseConnection, 1);
		}

		public bool Update()
		{
			try
			{
				OleDbCommand oOleDbCommand = new OleDbCommand();
				oOleDbCommand.CommandText = "uEmployee";
				oOleDbCommand.CommandType = CommandType.StoredProcedure;
				oOleDbCommand.Parameters.Add("@PersonID", OleDbType.VarChar, 50).Value = PersonID;
				oOleDbCommand.Parameters.Add("@PersonNbr", OleDbType.VarChar, 50).Value = PersonNbr;
				oOleDbCommand.Parameters.Add("@EmployeeNumber", OleDbType.BigInt).Value = EmployeeNumber;
				oOleDbCommand.Parameters.Add("@FirstName", OleDbType.VarChar, 50).Value = FirstName;
				oOleDbCommand.Parameters.Add("@MiddleName", OleDbType.VarChar, 50).Value = MiddleName;
				oOleDbCommand.Parameters.Add("@LastName", OleDbType.VarChar, 50).Value = LastName;
				oOleDbCommand.Parameters.Add("@Title", OleDbType.VarChar, 250).Value = Title;
				oOleDbCommand.Parameters.Add("@FamiliarName", OleDbType.VarChar, 250).Value = FamiliarName;
				oOleDbCommand.Parameters.Add("@Suffix", OleDbType.VarChar, 50).Value = Suffix;
				oOleDbCommand.Parameters.Add("@LegacyEmployeeNumber", OleDbType.VarChar, 50).Value = LegacyEmployeeNumber;
				oOleDbCommand.Parameters.Add("@WorkEMail", OleDbType.VarChar, 250).Value = WorkEMail;
				oOleDbCommand.Parameters.Add("@WorkPhone", OleDbType.VarChar, 50).Value = WorkPhone;
				oOleDbCommand.Parameters.Add("@WorkMobilePhone", OleDbType.VarChar, 50).Value = WorkMobilePhone;
				oOleDbCommand.Connection = moDatabaseConnection;
				oOleDbCommand.ExecuteNonQuery();
				return true;
			}
			catch (Exception oException)
			{
				ErrorMessage = oException.Message;
				return false;
			}
		}

		public void CloseDatabase()
		{
			DBRoutines oDBRoutines = new DBRoutines();
			oDBRoutines.CloseDB(moDatabaseConnection);
		}






		//
	}
}
