﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;

namespace OracleHR
{
	class Processed
	{
		public int ProcessedTypeID{get;set;}
		public DateTime StartDateTime { get; set; }
		public DateTime EndDateTime { get; set; }
		public string PathFileName { get; set; }
		public string BackupPathFileName { get; set; }
		public int EmployeeRecordCount { get; set; }
		public int WorkRelationshipRecordCount { get; set; }
		public int AssignmentRecordCount { get; set; }
		private OleDbConnection moDatabaseConnection;
		public string ErrorMessage { get; set; }

		public void InitializeData()
	{
		this.ProcessedTypeID = 0;
			this.StartDateTime = new DateTime();
			this.EndDateTime = new DateTime();
			this.PathFileName = null;
			this.BackupPathFileName = null;
			this.EmployeeRecordCount = 0;
			this.WorkRelationshipRecordCount = 0;
			this.AssignmentRecordCount = 0;
		}

	public bool OpenDatabase()
	{
		DBRoutines oDBRoutines = new DBRoutines();
		moDatabaseConnection = new OleDbConnection();
		return oDBRoutines.OpenDatabase(moDatabaseConnection, 1);
	}

	public bool Insert()
	{
		try
		{
			OleDbCommand oOleDbCommand = new OleDbCommand();
			oOleDbCommand.CommandText = "iProcessed";
			oOleDbCommand.CommandType = CommandType.StoredProcedure;
			oOleDbCommand.Parameters.Add("@ProcessedTypeID", OleDbType.BigInt).Value = ProcessedTypeID;
			oOleDbCommand.Parameters.Add("@StartDateTime", OleDbType.Date).Value = StartDateTime;
			oOleDbCommand.Parameters.Add("@EndDateTime", OleDbType.Date).Value = EndDateTime;
			oOleDbCommand.Parameters.Add("@PathFileName", OleDbType.VarChar, 500).Value = PathFileName;
			oOleDbCommand.Parameters.Add("@BackupPathFileName", OleDbType.VarChar, 500).Value = BackupPathFileName;
			oOleDbCommand.Parameters.Add("@EmployeeRecordCount", OleDbType.BigInt).Value = EmployeeRecordCount;
			oOleDbCommand.Parameters.Add("@WorkRelationshipRecordCount", OleDbType.BigInt).Value = WorkRelationshipRecordCount;
			oOleDbCommand.Parameters.Add("@AssignmentRecordCount", OleDbType.BigInt).Value = AssignmentRecordCount;
			oOleDbCommand.Connection = moDatabaseConnection;
			oOleDbCommand.ExecuteNonQuery();
			return true;
		}
		catch (Exception oException)
		{
			ErrorMessage = oException.Message;
			return false;
		}
	}

	public void CloseDatabase()
	{
		DBRoutines oDBRoutines = new DBRoutines();
		oDBRoutines.CloseDB(moDatabaseConnection);
	}





		//
	}
}
