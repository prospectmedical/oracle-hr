﻿using System;
using System.Data;
using System.Data.OleDb;

namespace OracleHR
{
	class WorkRelationship
	{
		public string WorkRelationshipID { get; set; }
		public string PersonID { get; set; }
		public string OriginalHireDate { get; set; }
		public string StartDate { get; set; }
		public string TerminationDate { get; set; }

		private OleDbConnection moDatabaseConnection;
		public string ErrorMessage { get; set; }

		public void InitializeData()
		{
			this.WorkRelationshipID = null;
			this.PersonID = null;
			this.OriginalHireDate = null;
			this.StartDate = null;
			this.TerminationDate = null;

		}

		public bool OpenDatabase()
		{
			DBRoutines oDBRoutines = new DBRoutines();
			moDatabaseConnection = new OleDbConnection();
			return oDBRoutines.OpenDatabase(moDatabaseConnection, 1);
		}

		public bool Update()
		{
			try
			{
				OleDbCommand oOleDbCommand = new OleDbCommand();
				oOleDbCommand.CommandText = "uWorkRelationship";
				oOleDbCommand.CommandType = CommandType.StoredProcedure;
				oOleDbCommand.Parameters.Add("@WorkRelationshipID", OleDbType.VarChar, 50).Value = WorkRelationshipID;
				oOleDbCommand.Parameters.Add("@PersonID", OleDbType.VarChar, 50).Value = PersonID;
				oOleDbCommand.Parameters.Add("@OriginalHireDate", OleDbType.VarChar, 50).Value = OriginalHireDate;
				oOleDbCommand.Parameters.Add("@StartDate", OleDbType.VarChar, 50).Value = StartDate;
				oOleDbCommand.Parameters.Add("@TerminationDate", OleDbType.VarChar, 50).Value = TerminationDate;
				oOleDbCommand.Connection = moDatabaseConnection;
				oOleDbCommand.ExecuteNonQuery();
				return true;
			}
			catch (Exception oException)
			{
				ErrorMessage = oException.Message;
				return false;
			}
		}

		public void CloseDatabase()
		{
			DBRoutines oDBRoutines = new DBRoutines();
			oDBRoutines.CloseDB(moDatabaseConnection);
		}






		//
	}
}
