﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Collections;
using System.Net.Mail;
using System.Net.Mime;

namespace OracleHR
{
	class Message
	{
		private static string msSender = null;
		private static string msRecipient = null;
		private static string msSubject = null;
		private static string msMessageText = null;
		private static string msLogoPicture01 = null;
		private static string msBCC = null;
		private static ArrayList malAttachments= new ArrayList();

		#region "Routines"
		private bool ValidEMailAddress(string EMailAddress)
		{
			Regex oRegex = new Regex(@"\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}");
			return oRegex.IsMatch(EMailAddress);
		}

		private string ConvertToText(string MessageText)
		{
			string ReturnValue = MessageText;
			ReturnValue = ReturnValue.Replace("&nbsp;", " ");
			ReturnValue = ReturnValue.Replace("<br />", Environment.NewLine);
			ReturnValue = ReturnValue.Replace("&bull;", Char.ToString((char)149));
			ReturnValue = ReturnValue.Replace("<font size=" + Global.DoubleQuote + "1" + Global.DoubleQuote + ">", null);
			ReturnValue = ReturnValue.Replace("<font size=" + Global.DoubleQuote + "2" + Global.DoubleQuote + ">", null);
			ReturnValue = ReturnValue.Replace("<font size=" + Global.DoubleQuote + "3" + Global.DoubleQuote + ">", null);
			ReturnValue = ReturnValue.Replace("<font size=" + Global.DoubleQuote + "4" + Global.DoubleQuote + ">", null);
			ReturnValue = ReturnValue.Replace("<font size=" + Global.DoubleQuote + "5" + Global.DoubleQuote + ">", null);
			ReturnValue = ReturnValue.Replace("<font size=" + Global.DoubleQuote + "6" + Global.DoubleQuote + ">", null);
			ReturnValue = ReturnValue.Replace("<font size=" + Global.DoubleQuote + "7" + Global.DoubleQuote + ">", null);
			ReturnValue = ReturnValue.Replace("<font color=" + Global.DoubleQuote + "#00ABC2" + Global.DoubleQuote + ">", null);
			ReturnValue = ReturnValue.Replace("<font color=" + Global.DoubleQuote + "#E3502D" + Global.DoubleQuote + ">", null);
			ReturnValue = ReturnValue.Replace("<font color=" + Global.DoubleQuote + "#777777" + Global.DoubleQuote + ">", null);
			ReturnValue = ReturnValue.Replace("<font color=" + Global.DoubleQuote + "#000000" + Global.DoubleQuote + ">", null);
			ReturnValue = ReturnValue.Replace("<font color=" + Global.DoubleQuote + "#FFFFFF" + Global.DoubleQuote + ">", null);
			ReturnValue = ReturnValue.Replace("</font>", null);
			return ReturnValue;
		}

		private void LinkImage(string CID, string ImageFile, string MediaType, AlternateView View)
		{
			LinkedResource oLinkedResource;

			if (msMessageText.Contains(CID) && ImageFile.Length > 0)
			{
				oLinkedResource = new LinkedResource(ImageFile, MediaType);
				oLinkedResource.ContentId = CID;
				oLinkedResource.ContentType.MediaType = MediaType;
				oLinkedResource.TransferEncoding = TransferEncoding.Base64;
				oLinkedResource.ContentType.Name = oLinkedResource.ContentId;
				oLinkedResource.ContentLink = new Uri("cid:" + oLinkedResource.ContentId);
				View.LinkedResources.Add(oLinkedResource);
			}
		}

		private string AddImageTag(string CID, string ImageTag)
		{
			string ReturnValue = CID;
			if (ImageTag.Length > 0 && CID.Length > 2)
			{
				ReturnValue = CID.Substring(0, CID.Length - 2) + ImageTag + CID.Substring(CID.Length - 2, 2);
			}
			return ReturnValue;
		}
		#endregion

		public bool Sender(string EMailAddress)
		{
			if (EMailAddress.Length > 0 && ValidEMailAddress(EMailAddress))
			{
				msSender = EMailAddress;
				return true;
			}
			else
			{
				msSender = null;
				return false;
			}
		}

		public bool Recipient(string EMailAddress)
		{
			if (!ValidEMailAddress(EMailAddress)) { return false; }
			if (EMailAddress.Length <= 0)
			{
				msRecipient = null;
				return true;
			}
			if (msRecipient != null && msRecipient.Length > 0) { msRecipient += "; "; }
			msRecipient += EMailAddress;
			return true;
		}

		public bool BCC(string EMailAddress)
		{
			if (!ValidEMailAddress(EMailAddress)) { return false; }
			if (EMailAddress.Length <= 0)
			{
				msBCC = null;
				return true;
			}
			if (msBCC != null && msBCC.Length > 0) { msBCC += "; "; }
			msBCC += EMailAddress;
			return true;
		}

		public bool Subject(string SubjectText)
		{
			msSubject = SubjectText;
			return true;
		}

		public bool MessageText(string InMessageText)
		{
			msMessageText = InMessageText;
			return true;
		}

		public bool AddAttachment(string PathFileName)
		{
			string sPathFileName = PathFileName.Trim();
			FileInfo oFileInfo;

			if (sPathFileName.Length <= 0) { return true; }
			oFileInfo = new FileInfo(sPathFileName);
			if (!oFileInfo.Exists) { return false; }
			malAttachments.Add(PathFileName);
			return true;
		}

		public bool Logo01PathFileName(string PathFileName)
		{
			string sPathFileName = PathFileName.Trim();
			FileInfo oFileInfo;

			if (sPathFileName.Length <= 0) { return true; }
			oFileInfo = new FileInfo(sPathFileName);
			if (!oFileInfo.Exists) { return false; }
			msLogoPicture01 = PathFileName;
			return true;
		}

		public bool Send(string ImageTag = null)
		{
			SmtpClient oSmtpClient = new SmtpClient(Global.SMTPAddress);
			MailMessage oMailMessage;
			AlternateView oPlainView;
			AlternateView oHTMLView;
			FileInfo oFileInfo;

			if (msRecipient == null || msSender == null) { return false; }
			oMailMessage = new MailMessage(new MailAddress(msSender), new MailAddress(msRecipient));
			if (msBCC != null && msBCC.Length > 0)
			{
				oMailMessage.Bcc.Add(msBCC);
			}
			oMailMessage.Subject = msSubject;
			oPlainView = AlternateView.CreateAlternateViewFromString(ConvertToText(msMessageText), Encoding.UTF8, MediaTypeNames.Text.Plain);
			oHTMLView = AlternateView.CreateAlternateViewFromString("<html><body>" + msMessageText + "</body></html>", Encoding.UTF8, MediaTypeNames.Text.Html);
			LinkImage(AddImageTag("logo01id", ImageTag), msLogoPicture01, MediaTypeNames.Image.Jpeg, oHTMLView);
			oMailMessage.Priority = MailPriority.Normal;
			if (malAttachments.Count > 0)
			{
				for (int iWork = 0; iWork < malAttachments.Count; iWork += 1)
				{
					oFileInfo = new FileInfo(malAttachments[iWork].ToString());
					switch (oFileInfo.Extension.ToLower())
					{
						case "pdf":
							oMailMessage.Attachments.Add(new Attachment(malAttachments[iWork].ToString(), MediaTypeNames.Application.Pdf));
							break;
						case "rtf":
							oMailMessage.Attachments.Add(new Attachment(malAttachments[iWork].ToString(), MediaTypeNames.Application.Rtf));
							break;
						case "zip":
							oMailMessage.Attachments.Add(new Attachment(malAttachments[iWork].ToString(), MediaTypeNames.Application.Zip));
							break;
						default:
							oMailMessage.Attachments.Add(new Attachment(malAttachments[iWork].ToString(), MediaTypeNames.Application.Octet));
							break;
					}
				}
			}
			oMailMessage.AlternateViews.Add(oPlainView);
			oMailMessage.AlternateViews.Add(oHTMLView);
			oMailMessage.IsBodyHtml = true;
			oSmtpClient.Send(oMailMessage);
			return true;
		}

		//
	}
}
