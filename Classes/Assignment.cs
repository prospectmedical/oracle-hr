﻿using System;
using System.Data;
using System.Data.OleDb;

namespace OracleHR
{
	class Assignment
	{
		public string AssignmentID { get; set; }
		public string PersonID { get; set; }
		public string EffectiveStartDate { get; set; }
		public string AssignmentNbr { get; set; }
		public string PrimaryFlag { get; set; }
		public string AssignmentStatusCode { get; set; }
		public string AssignmentStatusName { get; set; }
		public string EmploymentCategoryCode { get; set; }
		public string EmploymentCategoryName { get; set; }
		public string JobCode { get; set; }
		public string JobName { get; set; }
		public string DepartmentName { get; set; }
		public string LocationCode { get; set; }
		public string LocationName { get; set; }
		public string ReportingCompany { get; set; }
		public string PositionCode { get; set; }
		public string CATSCompanyID { get; set; }
		public string CATSDepartmentNumber { get; set; }
		public string CATSCostCenter { get; set; }
		public string CATSPositionNumber { get; set; }
		public string PositionName { get; set; }
		public string BargainingUnitCode { get; set; }

		private OleDbConnection moDatabaseConnection;
		public string ErrorMessage { get; set; }

		public void InitializeData()
		{
			this.AssignmentID = null;
			this.PersonID = null;
			this.EffectiveStartDate = null;
			this.AssignmentNbr = null;
			this.PrimaryFlag = null;
			this.AssignmentStatusCode = null;
			this.AssignmentStatusName = null;
			this.EmploymentCategoryCode = null;
			this.EmploymentCategoryName = null;
			this.JobCode = null;
			this.JobName = null;
			this.DepartmentName = null;
			this.LocationCode = null;
			this.LocationName = null;
			this.ReportingCompany = null;
			this.PositionCode  = null;
			this.CATSCompanyID = null;
			this.CATSDepartmentNumber = null;
			this.CATSCostCenter = null;
			this.CATSPositionNumber = null;
			this.PositionName = null;
			this.BargainingUnitCode = null;
		}

		public bool OpenDatabase()
		{
			DBRoutines oDBRoutines = new DBRoutines();
			moDatabaseConnection = new OleDbConnection();
			return oDBRoutines.OpenDatabase(moDatabaseConnection, 1);
		}

		public bool Update()
		{
			try
			{
				OleDbCommand oOleDbCommand = new OleDbCommand();
				oOleDbCommand.CommandText = "uAssignment";
				oOleDbCommand.CommandType = CommandType.StoredProcedure;
				oOleDbCommand.Parameters.Add("@AssignmentID", OleDbType.VarChar, 50).Value = AssignmentID;
				oOleDbCommand.Parameters.Add("@PersonID", OleDbType.VarChar, 50).Value = PersonID;
				oOleDbCommand.Parameters.Add("@EffectiveStartDate", OleDbType.VarChar, 25).Value = EffectiveStartDate;
				oOleDbCommand.Parameters.Add("@AssignmentNbr", OleDbType.VarChar, 50).Value = AssignmentNbr;
				oOleDbCommand.Parameters.Add("@PrimaryFlag", OleDbType.VarChar, 25).Value = PrimaryFlag;
				oOleDbCommand.Parameters.Add("@AssignmentStatusCode", OleDbType.VarChar, 50).Value = AssignmentStatusCode;
				oOleDbCommand.Parameters.Add("@AssignmentStatusName", OleDbType.VarChar, 50).Value = AssignmentStatusName;
				oOleDbCommand.Parameters.Add("@EmploymentCategoryCode", OleDbType.VarChar, 50).Value = EmploymentCategoryCode;
				oOleDbCommand.Parameters.Add("@EmploymentCategoryName", OleDbType.VarChar, 250).Value = EmploymentCategoryName;
				oOleDbCommand.Parameters.Add("@JobCode", OleDbType.VarChar, 50).Value = JobCode;
				oOleDbCommand.Parameters.Add("@JobName", OleDbType.VarChar, 250).Value = JobName;
				oOleDbCommand.Parameters.Add("@DepartmentName", OleDbType.VarChar, 250).Value = DepartmentName;
				oOleDbCommand.Parameters.Add("@LocationCode", OleDbType.VarChar, 50).Value = LocationCode;
				oOleDbCommand.Parameters.Add("@LocationName", OleDbType.VarChar, 25).Value = LocationName;
				oOleDbCommand.Parameters.Add("@ReportingCompany", OleDbType.VarChar, 250).Value = ReportingCompany;
				oOleDbCommand.Parameters.Add("@PositionCode", OleDbType.VarChar, 250).Value = PositionCode;
				oOleDbCommand.Parameters.Add("@CATSCompanyID", OleDbType.VarChar, 15).Value = CATSCompanyID;
				oOleDbCommand.Parameters.Add("@CATSDepartmentNumber", OleDbType.VarChar, 15).Value = CATSDepartmentNumber;
				oOleDbCommand.Parameters.Add("@CATSCostCenter", OleDbType.VarChar, 15).Value = CATSCostCenter;
				oOleDbCommand.Parameters.Add("@CATSPositionNumber", OleDbType.VarChar, 15).Value = CATSPositionNumber;
				oOleDbCommand.Parameters.Add("@PositionName", OleDbType.VarChar, 250).Value = PositionName;
				oOleDbCommand.Parameters.Add("@BargainingUnitCode", OleDbType.VarChar, 50).Value = BargainingUnitCode;
				oOleDbCommand.Connection = moDatabaseConnection;
				oOleDbCommand.ExecuteNonQuery();
				return true;
			}
			catch (Exception oException)
			{
				ErrorMessage = oException.Message;
				return false;
			}
		}

		public void CloseDatabase()
		{
			DBRoutines oDBRoutines = new DBRoutines();
			oDBRoutines.CloseDB(moDatabaseConnection);
		}






		//
	}
}
