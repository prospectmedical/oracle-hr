﻿namespace OracleHR
{
	partial class OracleHRService
	{
		private System.ComponentModel.IContainer components = null;

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.CanShutdown = true;
			this.ServiceName = "OracleHR";
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}


	}
}
