﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections;

namespace OracleHR
{
	class InitializeApplication
	{
		public string LogPath { get; set; }
		public bool TraceEnabled { get; set; }
		public double ServicePauseTime { get; set; }
		public ArrayList EMailRecipient { get; set; }
		public string EMailSender { get; set; }
		public bool SendEmail { get; set; }
		public string NorthPointPath { get; set; }
		public string NorthPointFilePattern { get; set; }

		public bool GetSettings()
		{
			PublicRoutine oPublicRoutine = new PublicRoutine();
			FileStream oFileStream = new FileStream(Global.ApplicationPath + "OracleHR.ini", FileMode.Open, FileAccess.Read);
			StreamReader oStreamReader = new StreamReader(oFileStream);
			string InRecord;
			int iWork;
			string ItemType;
			string ItemSetting;
			int INICounter = 0;
			int INICount = 8;
			bool EMailRecipientFound = false;

			EMailRecipient = new ArrayList();
			oStreamReader.BaseStream.Seek(0, SeekOrigin.Begin);
			while (oStreamReader.Peek() > -1)
			{
				InRecord = oStreamReader.ReadLine().Trim();
				if (InRecord.Length > 0)
				{
					if (InRecord.Substring(0, 1) != ";")
					{
						if (InRecord.IndexOf(";") > 0)
						{
							InRecord = InRecord.Substring(0, InRecord.IndexOf(";"));
							InRecord = InRecord.Trim();
						}
						iWork = InRecord.IndexOf(" ");
						if (iWork > 3 && iWork < InRecord.Length)
						{
							ItemType = InRecord.Substring(0, iWork ).ToLower().Trim();
							ItemSetting = InRecord.Substring(iWork + 1, InRecord.Length - iWork - 1).ToLower().Trim();
							switch (ItemType)
							{
								case "[logpath]":
									LogPath = ItemSetting;
									INICounter += 1;
									break;

								case "[traceenabled]":
									if (ItemSetting.ToLower() == "true")
									{	TraceEnabled = true; }
									else
									{ TraceEnabled = false; }
									INICounter += 1;
									break;

								case "[servicepausetime]":
									ServicePauseTime = Double.Parse(ItemSetting);
									INICounter += 1;
									break;

								case "[emailrecipient]":
									EMailRecipient.Add(ItemSetting);
									if (!EMailRecipientFound)
									{
										EMailRecipientFound = true;
										INICounter += 1;
									}
									break;

								case "[emailsender]":
									EMailSender = ItemSetting;
									INICounter += 1;
									break; 

								case "[sendemail]":
									if (ItemSetting.ToLower() == "true")
									{ SendEmail = true; }
									else
									{ SendEmail = false; }
									INICounter += 1;
									break;

								case "[northpointpath]":
									NorthPointPath = ItemSetting;
									INICounter += 1;
									break;

								case "[northpointfilepattern]":
									NorthPointFilePattern = ItemSetting;
									INICounter += 1;
									break;
							}
						}
					}
				}
			}
			oStreamReader.Close();
			oFileStream.Close();
			if (INICount != INICounter)
			{
				oPublicRoutine.WriteEventLog("InitializeApplication", "Error: Incorrect ini count, entries are missing.", EventLogEntryType.Error);
				return false;
			}
			else
			{
				oPublicRoutine.WriteEventLog("InitializeApplication", "Complete", EventLogEntryType.Error);
			}
			return true;
		}








		//---------------
	}
}
