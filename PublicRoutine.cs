﻿using System;
using System.IO;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace OracleHR
{
	class PublicRoutine
	{
		public string ApplicationStartupPath()
		{
			Assembly oAssembly = Assembly.GetExecutingAssembly();
			string ReturnValue = Path.GetDirectoryName(oAssembly.Location);

			if (ReturnValue.Length > 4) { if (ReturnValue.ToLower().Substring(ReturnValue.Length - 4, 4) == @"\bin") { ReturnValue = ReturnValue.Substring(0, ReturnValue.Length - 4); } }
			if (ReturnValue.Length > 10) { if (ReturnValue.ToLower().Substring(ReturnValue.Length - 10, 10) == @"\bin\debug") { ReturnValue = ReturnValue.Substring(0, ReturnValue.Length - 10); } }
			if (ReturnValue.Length > 12) { if (ReturnValue.ToLower().Substring(ReturnValue.Length - 12, 12) == @"\bin\release") { ReturnValue = ReturnValue.Substring(0, ReturnValue.Length - 12); } }
			return AddBackslash(ReturnValue);
		}

		public string AddBackslash(string Path)
		{
			string ReturnValue = Path;
			if (ReturnValue.Length > 1) { if (ReturnValue.Substring(ReturnValue.Length - 1, 1) != @"\") { ReturnValue += @"\"; } }
			return ReturnValue;
		}

		public void TraceWrite(string Message, string Category)
		{
			if (Global.TraceEnabled)
			{
				string sCategory = Category;
				if (sCategory.Length < 23) { sCategory += string.Concat(Enumerable.Repeat(".", 32 - sCategory.Length)); }
				Trace.WriteLine(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + " - " + Message, sCategory);
			}
		}

		public void WriteEventLog(string Message, string Category, EventLogEntryType EventType)
		{
			EventLog oEventLog = new EventLog();
			if (!EventLog.Exists(Global.EventLogName)){ EventLog.CreateEventSource(Global.ApplicationName, Global.EventLogName); }
			oEventLog.Source = Global.EventLogName;
			oEventLog.WriteEntry("[" + Category + "]" + Message, EventType);
		}

		public void WriteEventTrace(string Message, string Category, EventLogEntryType EventType)
		{
			WriteEventLog(Message, Category, EventType);
			TraceWrite(Message, Category);
		}

		public void CloseFileStream(FileStream InFileStream)
		{
			try
			{
				InFileStream.Close();
				InFileStream.Dispose();
				InFileStream = null;
			}
			finally { }
		}

		public void CloseStreamReader(StreamReader InStreamReader)
		{
			try
			{
				InStreamReader.Close();
				InStreamReader.Dispose();
				InStreamReader = null;
			}
			finally { }
		}

		public string ASPCRLF(int CharacterCount)
		{
			string ReturnValue = null;

			if (CharacterCount > 0)
			{
				for (int iWork = 0; iWork < CharacterCount; iWork++)
				{
					ReturnValue += "<br />";
				}
			}
			return ReturnValue;
		}

		public string ASPSpace(int CharacterCount)
		{
			string ReturnValue = null;

			if (CharacterCount > 0)
			{
				for (int iWork = 0; iWork < CharacterCount; iWork++)
				{
					ReturnValue += "&nbsp;";
				}
			}
			return ReturnValue;
		}


		//-----------
	}
}
